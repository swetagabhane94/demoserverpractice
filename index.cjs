const express = require('express')
const app = express()
const PORT = process.env.PORT || 5001

app.get('/',(request,response)=>{
    response.send("Checking Demo1 Server")
})
app.listen(PORT,()=>{
    console.log(`listening app gives http://localhost:${PORT}`)
})
